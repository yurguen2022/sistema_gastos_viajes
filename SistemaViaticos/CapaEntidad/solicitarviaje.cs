﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class solicitarviaje
    {
        public int idsolicitud { get; set; }

        public String nombreviatico { get; set; }
        public usuarios nombreusuario {get;set;}
        public proyecto nombreproyecto{ get; set; }
        public String fechasalida { get; set; }
        public String fechacaptura { get; set; }
        public String fecharegreso { get; set; }
        public uniAdmin nombreuniadmin { get; set; }
        public centrodecostos centrocosto { get; set; }
        public String comentarios { get; set; }
        public String origen { get; set; }
        public String destino { get; set; }
        public String estatus { get; set; }
        public String eliminar { get; set; }
        public decimal importe { get; set; }
    }
}
