﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data.SqlClient;
using System.Data;

namespace backendDatos
{
    public class cr_cuentagastos
    {
        public List<cuentagastos> listargastos()
        {
            List<cuentagastos> gasto = new List<cuentagastos>();
            try
            {
                using (SqlConnection conexionnueva = new SqlConnection(Conexion.con))
                {
                    String query = "select id, nombre from cuentagastos";
                    SqlCommand cmd = new SqlCommand(query, conexionnueva);
                    cmd.CommandType = CommandType.Text;
                    conexionnueva.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            gasto.Add(new cuentagastos()
                            {
                                idgasto = Convert.ToInt16(dr["id"]),
                                nombre = dr["nombre"].ToString()
                            }
                         );
                      }
                    }
                }
            }
            catch (Exception e)
            {
                gasto = new List<cuentagastos>();
            }
            return gasto;
        }

        public int registrargastos(cuentagastos obj, out string Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                   // string query = String.Format("IF NOT EXISTS (SELECT * FROM cuentagastos where nombre='{0}')insert into cuentagastos(nombre) values('{0}')", obj.nombre);
                    SqlCommand cmd = new SqlCommand("cp_registrargasto", oconexion);
                    cmd.Parameters.AddWithValue("nombre", obj.nombre);
                    cmd.Parameters.Add("Resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("Mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["Resultado"].Value);
                    Mensaje = cmd.Parameters["Mensaje"].Value.ToString();

                }


            }
            catch (Exception e)

            {
                idautogenerado = 0;
               
                Mensaje = e.Message;

            }
            return idautogenerado
                ;
        }
        public bool eliminargastos(int id, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                   
                {
                    SqlCommand cmd = new SqlCommand("cp_eliminarcuentagasto", oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.Add("Resultado", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("Mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();

                    resultado = Convert.ToBoolean(cmd.Parameters["Resultado"].Value);
                    Mensaje = cmd.Parameters["Mensaje"].Value.ToString();

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }
        public bool editargastos(cuentagastos obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1)cuentagastos set nombre ='{0}'where id =@id", obj.nombre);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.idgasto);
                    cmd.Parameters.AddWithValue("@nombre", obj.nombre);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }
    }
}
