﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;


namespace CapaEmpleado
{
   public class cn_contableempleado
    {
        private cr_contaempleados contablempleados = new cr_contaempleados();

        public List<contableempleado> Listarcontaemple(int id)
        {

            return contablempleados.listarcontableempleado(id);
        }


        public total Listitacontable(int id)
        {

            return contablempleados.listarconta(id);
        }

        public int Registrarcontableempleado(contableempleado obj, out String Mensaje)
        {
            Mensaje = string.Empty;
          if (obj.clavecontable.id==0)
            {
                Mensaje = "la clave contable no puede ser vacio";
            }
            else if (obj.clavesolicitud.idsolicitud==0)
            {
                Mensaje = "la clave de proyecto no puede estar vacio";

            }




            if (string.IsNullOrEmpty(Mensaje))
            {
                return contablempleados.registrarcontableempleado( obj, out Mensaje);

            }
            else
            {
                return 0;



            }

        }

        public bool editarcuentascontables(contableempleado obj, out String Mensaje)
        {
            Mensaje = string.Empty;

            if (obj.clavecontable.id == 0)
            {
                Mensaje = "la clave contable no puede ser vacio";
            }
            else if (obj.monto == 0)
            {
                Mensaje = "el monto no puede estar vacio";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
               return contablempleados.editarcontaempleados(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool editarcomprobante(contableempleado obj, out String Mensaje)
        {
            Mensaje = string.Empty;

            if (string.IsNullOrEmpty(obj.estatus) || string.IsNullOrWhiteSpace(obj.estatus))
            {
                Mensaje = "el estatus es vacio";
            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return contablempleados.editarcomprobante (obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimarcuentascontables(int id, out string Mensaje)
        {
            return contablempleados.Elimarcontablempleado(id, out Mensaje);

        }
    }
}

