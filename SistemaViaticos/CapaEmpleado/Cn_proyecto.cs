﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;

namespace CapaEmpleado
{
    public class Cn_proyecto
    {
        private cr_proyecto objbackenddatos = new cr_proyecto();

        public List<proyecto> Listarproyecto2()
        {

            return objbackenddatos.Listarproyecto();
        }

        public int Registrarproyecto(proyecto obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre del proyecto es vacio";

            }
            else
            if (string.IsNullOrEmpty(obj.descripcion) || string.IsNullOrWhiteSpace(obj.descripcion))
            {
                Mensaje = "la descripcion del proyecto es vacio";

            }


            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.RegistrarProyecto(obj, out Mensaje);

            }
            else
            {
                return 0;


            }
          
        }

        public bool editarproyecto(proyecto obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre del proyecto es vacio";

            }
            else
           if (string.IsNullOrEmpty(obj.descripcion) || string.IsNullOrWhiteSpace(obj.descripcion))
            {
                Mensaje = "la descripcion del proyecto es vacio";

            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarproyecto(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimarproyecto(int id, out string Mensaje)
        {
            return objbackenddatos.eliminarproyecto(id, out Mensaje);
        }
    }
}
