﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;


namespace CapaEmpleado
{
   public  class Cn_unidadmin
    {

        private cr_uniadmin objbackenddatos = new cr_uniadmin();

        public List<uniAdmin> Listaruniadmin()
        {

            return objbackenddatos.listaruniadmin();
        }

        public int Registrarunidadadmin(uniAdmin obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre  es vacio";

            }
            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.registrarunidadamin(obj, out Mensaje);

            }
            else
            {
                return 0;



            }

        }

        public bool editargasto(uniAdmin obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre  es vacio";

            }


            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarunidadadmin(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimaruniadmino(int id, out string Mensaje)
        {
            return objbackenddatos.eliminaruniadmin(id, out Mensaje);
        }
    }
}
